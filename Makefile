PREFIX ?= /usr/bin

.PHONY: clean deb install uninstall

clean:
	rm -r target/

deb:
	sudo deploy/build-deb.sh

install:
#	./install
	dpgk -i target/later-*.deb

uninstall:
	dpkg –purge later


