"""
This plugin provides ls pretty printed and ordered
"""
from __future__ import print_function

_HOOKS=None

def issue_compare_3(e):
	if "priority" in e.properties:
		return int(e.properties["priority"])
	return -1


def shortString(self):
	"""One line representation"""
	i = self.msg.find("\n")
	if i < 0:
		i = None
	string = self.msg[:i]
	status = self.properties.get("status", "?")
	return " %s  %-50s %s"%(self.guid[:8], string, status) 

def cmd_ls(args):
	"""By default lists all issues, which are not closed."""
	

	issues = (_HOOKS.be_load_issue(guid) for guid in _HOOKS.be_all_guids())

	# Filtering for bugs
	if args.count('bug'):
		issues = (iss for iss in issues if "type" in iss.properties and iss.properties["type"] == "bug")
		args.remove('bug')

	if args.count('feat'):
		issues = (iss for iss in issues if "type" in iss.properties and iss.properties["type"] == "feat")
		args.remove('feat')

	issues = (iss for iss in issues if iss.properties["status"] != "closed")

	issues = sorted(issues, key=issue_compare_3, reverse=True )

	for issue in issues:
		if "priority" in issue.properties and int(issue.properties["priority"]) > 8:
			print("\033[31m" + issue.shortString() + "\033[0m")
		elif "priority" in issue.properties and int(issue.properties["priority"]) >= 2:
			print("\033[93m" + issue.shortString() + "\033[0m")
		elif "priority" in issue.properties and int(issue.properties["priority"]) >= 1:
			print("\033[32m" + issue.shortString() + "\033[0m")
		else:
			print("\033[02m" + issue.shortString() + "\033[0m")



def plugin_init(hooks):
	global _HOOKS
	hooks["cmd_ls"] = cmd_ls
	_HOOKS = hooks

